FROM python:3.10

ADD openai_api.py /
ADD requirements.txt /
RUN pip install -r requirements.txt

ENTRYPOINT [ "python", "./openai_api_publisher.py" ]
