# openai-api-publisher

## Introduction

Contains a basic approach to use OPEN AI API using Python.

The script receives the message by parameter and process the response as required to be published in Twitter.

The parsed content will be sent to a RabbitMQ queue.

## Parameters

- **-mesage**: Contains the message to the OPEN AI API.
- **-queue_name**: Contains the queue name to publish the content in RabbitMQ.
- **-thread**: Contains the flag to parse the content in a thread format.

## Usage

### Docker

- Build the image
  `docker build --build-arg OPENAI_API_KEY -t openai-api-publisher .`

- Run the image
  `docker run openai-api-publisher -message MESSAGE -queue_name QUEUE_NAME -thread`

### Python

To use this script you can use the following command:

`python openai-api-publisher.py -message MESSAGE -queue_name QUEUE_NAME -thread`

**NOTE**: Remember that you need to install the **openai** Python library and set up the API KEY as environment variable.

## Contact

If you have any question, you can contact me directly to `jherreroa23@gmail.com`
